# farmacias-ui
MVP de interfaz gráfica para muestra de farmacias en turno, región metropolitana. Desarrollado con [svelte](https://svelte.dev/).

### Requisitos
NodeJS versión 12

npm para instalación de dependencias

Docker para ejecución dentro de stack de servicios

### Instalación de dependencias
```bash
npm install
```

### Modo desarrollo
```bash
npm run dev
```

Navegar a [http://127.0.0.1:5000](http://127.0.0.1:5000). Para probar la integración con el microservicio de farmacias, este debe estar corriendo (intrucciones en el repositorio.)

### Docker
Construir imagen para ejecutar dentro de farmacias-stack, en el principal de la carpeta:
```bash
docker build . -t farmacias-ui
```

Para ejecutar como stack de servcios ver repositorio [farmacias-stack](https://gitlab.com/glpunk/farmacias-stack)