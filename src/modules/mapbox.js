import mapbox from 'mapbox-gl';

// https://docs.mapbox.com/help/glossary/access-token/
mapbox.accessToken = 'pk.eyJ1IjoiZ2xwdW5rIiwiYSI6ImNrNHJvajc4dTEwYXczbG10Z3dlZWc4NHUifQ.kl2PmndLhWVhWM9mg6zhfQ';

const key = {};

export { mapbox, key };